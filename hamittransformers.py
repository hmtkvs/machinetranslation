# -*- coding: utf-8 -*-

# Commented out IPython magic to ensure Python compatibility.
# %%capture
# !pip install simpletransformers

import logging
import pandas as pd
from simpletransformers.t5 import T5Model, T5Args
from sklearn.model_selection import train_test_split
import json
import sys
from datasets import Dataset

logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

# Load data
hf_dataset = Dataset.load_from_disk("opus_books/train")
df = hf_dataset.to_pandas()

df = df.rename(columns={"Spanish":"input_text", "Italian":"target_text"})
df = df[['input_text', 'target_text']]
df['prefix'] = "translate Spanish to Italian"
train_df, eval_df = train_test_split(df, test_size=0.2)
print(f"train shape: {train_df.shape}, eval shape: {eval_df.shape}")

# Model args
model_args = T5Args()
model_args.max_seq_length = 196
model_args.train_batch_size = 8
model_args.eval_batch_size = 8
model_args.num_train_epochs = 2
model_args.evaluate_during_training = False
model_args.use_multiprocessing = False
model_args.fp16 = False
model_args.save_steps = -1
model_args.save_eval_checkpoints = False
model_args.save_model_every_epoch = False
model_args.no_cache = True
model_args.reprocess_input_data = True
model_args.overwrite_output_dir = True
model_args.num_return_sequences = 1
model_args.wandb_project = False #"MT5 machine translation"

# Load model
model = T5Model("mt5", "google/mt5-xl", args=model_args, use_cuda = True)

# Train the model
model.train_model(train_df, eval_data=eval_df)

# Evaluating With F1 Score & Accuracy
from sklearn.metrics import f1_score, accuracy_score
def f1_multiclass(labels, preds):
    return f1_score(labels, preds, average='micro')

scores, model_outputs, wrong_predictions = model.eval_model(eval_df, f1=f1_multiclass, acc=accuracy_score)
print(f"Scores: {scores}")

# Predict to translate
spanish_input = ['Soy una profesional versátil que disfruta realizando tareas administrativas y de atención al cliente.',
                 'Quiero formar parte de un equipo de trabajo apoyándoles en la gestión, organización y administración.', 
                 'Me encantan las RRSS y los perros pequeños.']
predicted_italian, _ = model.predict(spanish_input)
print(f"Spanish input: {spanish_input}\nItalian output: {predicted_italian}")
